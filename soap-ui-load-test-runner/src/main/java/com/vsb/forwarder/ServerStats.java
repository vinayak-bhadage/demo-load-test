package com.vsb.forwarder;

import java.io.Serializable;

public class ServerStats implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double cpu;
	private double memory;
	private double disks;
	private double network;
	private String tag = "ServerStats";
	private String dateTime;

	public double getCpu() {
		return cpu;
	}

	public void setCpu(double cpu) {
		this.cpu = cpu;
	}

	public double getMemory() {
		return memory;
	}

	public void setMemory(double memory) {
		this.memory = memory;
	}

	public double getDisks() {
		return disks;
	}

	public void setDisks(double disks) {
		this.disks = disks;
	}

	public double getNetwork() {
		return network;
	}

	public void setNetwork(double network) {
		this.network = network;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	
}
