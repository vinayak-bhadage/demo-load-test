package com.vsb.forwarder;

import java.io.Serializable;

public class LoadTestResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String testStep;
	private int min;
	private int max;
	private double avg;
	private int last;
	private int cnt;
	private double tps;
	private int bytes;
	private int bps;
	private int err;
	private double rat;
	private String dateTime;
	private String loadTestId;
	private String tag = "LoadTestResult";
	private int threadCount;
	
	public String getTestStep() {
		return testStep;
	}

	public void setTestStep(String testStep) {
		this.testStep = testStep;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public double getAvg() {
		return avg;
	}

	public void setAvg(double avg) {
		this.avg = avg;
	}

	public int getLast() {
		return last;
	}

	public void setLast(int last) {
		this.last = last;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	public double getTps() {
		return tps;
	}

	public void setTps(double tps) {
		this.tps = tps;
	}

	public int getBytes() {
		return bytes;
	}

	public void setBytes(int bytes) {
		this.bytes = bytes;
	}

	public int getBps() {
		return bps;
	}

	public void setBps(int bps) {
		this.bps = bps;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getLoadTestId() {
		return loadTestId;
	}

	public void setLoadTestId(String loadTestId) {
		this.loadTestId = loadTestId;
	}

	public int getErr() {
		return err;
	}

	public void setErr(int err) {
		this.err = err;
	}

	public double getRat() {
		return rat;
	}

	public void setRat(double rat) {
		this.rat = rat;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public int getThreadCount() {
		return threadCount;
	}

	public void setThreadCount(int threadCount) {
		this.threadCount = threadCount;
	}
	
}
