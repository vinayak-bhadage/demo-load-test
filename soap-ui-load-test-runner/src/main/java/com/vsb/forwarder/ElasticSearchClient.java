package com.vsb.forwarder;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ElasticSearchClient {

	private static ObjectMapper mapper = new ObjectMapper();
	private Client client;
	public SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSSZZ");
	private String indexName =""; 
	public SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	private int threadCount;
	
	public ElasticSearchClient(String address, int port)
			throws UnknownHostException, IOException {
		super();
		
		Settings settings = ImmutableSettings.settingsBuilder()
		        .put("cluster.name", "demoloadtestelasticsearch").build();
		
		client = new TransportClient(settings).addTransportAddress(new InetSocketTransportAddress(address,
						port));
		indexName ="demo-load-test-"+ dateFormat.format(new Date());
		// socketClient = new Socket(address, port);
	}

	public void writeMessage(LoadTestResult loadTesResult) throws IOException {
		loadTesResult.setDateTime(simpleDateFormat.format(new Date()));
		loadTesResult.setThreadCount(threadCount);
		String sendMessage = mapper.writeValueAsString(loadTesResult);
		IndexResponse response = client.prepareIndex(indexName, "loadTesResult")
		        .setSource(sendMessage)
		        .execute()
		        .actionGet();
	}

	public void writeMessage(ServerStats result) throws IOException {
		result.setDateTime(simpleDateFormat.format(new Date()));
		String sendMessage = mapper.writeValueAsString(result);
		IndexResponse response = client.prepareIndex(indexName, "serverStats")				
		        .setSource(sendMessage)
		        .execute()
		        .actionGet();
	}

	public void closeClient() {
		if (client != null) {
			client.close();
		}
	}

	public int getThreadCount() {
		return threadCount;
	}

	public void setThreadCount(int threadCount) {
		this.threadCount = threadCount;
	}
	
}
