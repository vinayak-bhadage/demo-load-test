package com.vsb.loadtest;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.TableModel;

import com.eviware.soapui.impl.wsdl.loadtest.WsdlLoadTest;
import com.eviware.soapui.impl.wsdl.loadtest.data.LoadTestStatistics;
import com.eviware.soapui.tools.SoapUILoadTestRunner;
import com.vsb.forwarder.ElasticSearchClient;
import com.vsb.forwarder.LoadTestResult;

public class VexiereSoapUILoadTestRunner extends SoapUILoadTestRunner {
	private static int HeadCount = 0;

	private File file;
	private ElasticSearchClient forworder;
	private String logstashId;
	private static SimpleDateFormat formater = new SimpleDateFormat(
			"yyyyMMdd'T'HHmmss.SSSZ");

	public VexiereSoapUILoadTestRunner() {
		super();
	}

	public VexiereSoapUILoadTestRunner(String title) {
		super(title);
	}

	@Override
	protected void runWsdlLoadTest(WsdlLoadTest loadTest) {

		super.runWsdlLoadTest(loadTest);
		exportToExternalDataSource(loadTest.getStatisticsModel());
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void exportToExternalDataSource(LoadTestStatistics stats) {
		try {
			exportToFile(file, stats);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void exportToFile(File file, TableModel model) throws IOException {
		List<LoadTestResult> loadTestResultList = writeData(model);
		if (forworder != null && loadTestResultList != null) {
			for (LoadTestResult item : loadTestResultList) {
				forworder.writeMessage(item);
			}
		}
	}

	private List<LoadTestResult> writeData(TableModel model) {
		List<LoadTestResult> loadTestResultList = new ArrayList<LoadTestResult>();
		int c = 0;
		for (; c < model.getRowCount(); c++) {
			if (model.getColumnCount() > 0) {
				/*if (model.getValueAt(c, 1).toString()
						.startsWith("TestStep-")) {*/ // start with
					LoadTestResult loadTestResult = new LoadTestResult();
					loadTestResult.setLoadTestId(logstashId);
					loadTestResult.setDateTime(formater.format(new Date()));
					fillResult(model, c, loadTestResult);

					loadTestResultList.add(loadTestResult);			
			}
		}

		return loadTestResultList;
	}

	private void fillResult(TableModel model, int c,
			LoadTestResult loadTestResult) {
		for (int i = 1; i < model.getColumnCount(); i++) {
			switch (i) {
			case 1:
				loadTestResult.setTestStep(model.getValueAt(c, i)
						.toString());
				break;
			case 2:
				loadTestResult.setMin(Integer.parseInt(model
						.getValueAt(c, i).toString()));
				break;
			case 3:
				loadTestResult.setMax(Integer.parseInt(model
						.getValueAt(c, i).toString()));
				break;
			case 4:
				loadTestResult.setAvg(Double.parseDouble(model
						.getValueAt(c, i).toString()));
				break;
			case 5:
				loadTestResult.setLast(Integer.parseInt(model
						.getValueAt(c, i).toString()));
				break;
			case 6:
				loadTestResult.setCnt(Integer.parseInt(model
						.getValueAt(c, i).toString()));
				break;

			case 7:
				loadTestResult.setTps(Double.parseDouble(model
						.getValueAt(c, i).toString()));
				break;
			case 8:
				loadTestResult.setBytes(Integer.parseInt(model
						.getValueAt(c, i).toString()));
				break;
			case 9:
				loadTestResult.setBps(Integer.parseInt(model
						.getValueAt(c, i).toString()));
				break;
			case 10:
				loadTestResult.setErr(Integer.parseInt(model
						.getValueAt(c, i).toString()));
				break;
			case 11:
				loadTestResult.setRat(Double.parseDouble(model
						.getValueAt(c, i).toString()));
				break;
			}
		}
	}

	private void writerHeader(PrintWriter writer, TableModel model) {
		for (int i = 1; i < model.getColumnCount(); i++) {
			if (i > 1) {
				writer.print(',');
				System.out.print(',');
			}			
			writer.print(model.getColumnName(i));
		}

		writer.println();
		System.out.println();
	}

	public void setLogstashForworder(ElasticSearchClient forworder) {
		this.forworder = forworder;
	}

	public String getLogstashId() {
		return logstashId;
	}

	public void setLogstashId(String logstashId) {
		this.logstashId = logstashId;
	}

}
