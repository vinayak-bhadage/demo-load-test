package com.vsb.loadtest;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.eviware.soapui.SoapUI;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.model.testsuite.TestSuite;
import com.vsb.forwarder.ElasticSearchClient;

public class VexiereLoadTestMain {

	public static void main(String[] args) throws UnknownHostException,
			IOException, ParseException {
		int threadCount = 5;
		int limit = 100;

		Options options = new Options();

		options.addOption("lg", true,
				"Give the IP address and port no for logstash forwarder. i.e ip1:port1 ");

		options.addOption("tc", true, "Give the thread count default is 5");

		options.addOption("limit", true,
				"Give the limit for number of request default is 100");

		CommandLineParser parser = new BasicParser();

		CommandLine cmd = parser.parse(options, args);

		ElasticSearchClient forworder = null;

		if (cmd.hasOption("lg")) {
			String logstashIp = cmd.getOptionValue("lg");
			String host = logstashIp.split(":")[0];
			int port = Integer.parseInt(logstashIp.split(":")[1]);
			forworder = new ElasticSearchClient(host, port);
		}

		if (cmd.hasOption("tc")) {
			String threadCountValue = cmd.getOptionValue("tc");
			threadCount = Integer.parseInt(threadCountValue);
			forworder.setThreadCount(threadCount);
		}
		if (cmd.hasOption("limit")) {
			String limitValue = cmd.getOptionValue("limit");
			limit = Integer.parseInt(limitValue);
		}
		
		System.setProperty("soapui.log4j.config","src/main/resources/soapui-log4j.xml");
		VexiereSoapUILoadTestRunner runner = new VexiereSoapUILoadTestRunner(
				"SoapUI " + SoapUI.SOAPUI_VERSION + " Maven2 LoadTest Runner");

		String projectFileName ="src/main/resources/Greeting-soapui-project.xml";
		// runner.setProjectFile("src/main/resources/Vexiere-Test-soapui-project.xml");
		runner.setLogstashForworder(forworder);
		runner.setProjectFile(projectFileName);
		runner.setLogstashId("Benchmark release load test ");
	
		runner.setPrintReport(false);
		runner.setThreadCount(threadCount);
		runner.setLimit(limit);

		try {
			WsdlProject project = new WsdlProject(projectFileName);

			// retrieve all test suite from project
			List<TestSuite> testSuiteList = project.getTestSuiteList();

			// Iterate all TestSuites of project
			for (TestSuite testSuite : testSuiteList) {
				
				if (!testSuite.isDisabled()) {
					System.out.println("****Running Test suite "
							+ testSuite.getName() + "********");
					runner.runSuite(testSuite);
				}else{
					System.out.println("****Running Test suite "
							+ testSuite.getName() +  " disabled ********");
				}
			}

			System.out.println("****All Test suite completed "
				+ "********");
			// runner.run();
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		System.exit(0);
	}

}
