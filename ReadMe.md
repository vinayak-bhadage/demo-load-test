# Vexiere Load Test runner #

----------

This project is developed to get soapui load test result and it is then sent to elastic search through elastic search java client.

Pre requisite:

1. Maven 
3. JRE 1.7 and above 
4. Elastic search 1.5 and Kibana 4 setup is needed.
 
Following steps need to perform:

**Step 1:** Go to project directory ./soap-ui-load-test-runner
 

**Step 2:** Start soapui load test runner

	mvn clean install exec:java -Dexec.mainClass="com.vsb.loadtest.VexiereLoadTestMain" -Dexec.args="-lg localhost:9300 -tc 15 -limit 100"

	"tc" : 	"Give the thread count default is 5"
	"limit" :  "Give the limit for number of request default is 100"
	"lg" : Give the IP address and port no for elastic search forwarder. i.e ip1:port1 (localhost:9300)





**Note:** To execute it on jenkins remove the mvn part from all commands.


Use kibana to visualize the load test result.